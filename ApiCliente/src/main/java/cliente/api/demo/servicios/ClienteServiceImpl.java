/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.api.demo.servicios;

import cliente.api.demo.dao.ClienteDao;
import cliente.api.demo.modelos.Cliente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kirio
 */
@Service
public class ClienteServiceImpl implements ClienteService{
    @Autowired
    private ClienteDao dao;

    @Override
    @Transactional(readOnly = true)
    public List<Cliente> findAll() {
        return (List<Cliente>) dao.findAll();
    }

    @Override
    @Transactional
    public Cliente save(Cliente cliente) {
        return dao.save(cliente);
    }

    @Override
    @Transactional(readOnly = true)
    public Cliente findById(Long id) {
        return dao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void delete(Cliente cliente) {
        dao.delete(cliente);
    }
    
}
